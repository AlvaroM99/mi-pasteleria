const UglifyJsPlugin = require('uglifyjs-webpack-plugin')


new UglifyJsPlugin({
            "uglifyOptions":
                {
                    compress: {
                        warnings: false
                    },
                    sourceMap: true
                }
        }
    ),
